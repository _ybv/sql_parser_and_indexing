package edu.buffalo.cse.sql.index;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Iterator;

import edu.buffalo.cse.sql.Schema;
import edu.buffalo.cse.sql.SqlException;
import edu.buffalo.cse.sql.data.Datum;
import edu.buffalo.cse.sql.data.DatumBuffer;
import edu.buffalo.cse.sql.data.DatumSerialization;
import edu.buffalo.cse.sql.data.InsufficientSpaceException;
import edu.buffalo.cse.sql.buffer.BufferManager;
import edu.buffalo.cse.sql.buffer.BufferException;
import edu.buffalo.cse.sql.buffer.ManagedFile;
import edu.buffalo.cse.sql.buffer.FileManager;
import edu.buffalo.cse.sql.test.TestDataStream;

public class ISAMIndex implements IndexFile {
	public  ManagedFile mgdfile;
	public IndexKeySpec keySpec;
	public ISAMIndex(ManagedFile file, IndexKeySpec keySpec)
			throws IOException, SqlException
			{
		this.mgdfile=file;
		this.keySpec=keySpec;
		//throw new SqlException("Unimplemented");
			}

	public static ISAMIndex create(FileManager fm,
			File path,
			Iterator<Datum[]> dataSource,
			IndexKeySpec key)
					throws SqlException, IOException
					{
		ManagedFile mgdfile =fm.open(path);
		mgdfile.resize(0);
		mgdfile.ensureSize(1024);
		int indexpagel1=0;
		int pageno=1;
		int countforlevel1 = 1;
		HashMap<Integer,Integer> h1 = new HashMap<Integer,Integer>();
		int[] pagearray= new int[100000];//change later

		ByteBuffer z=null;
		DatumBuffer dbf=null;
		Iterator<Datum[]> td = dataSource;
		//	int i=1;
		int level = 0;
		while(td.hasNext())
		{  
			Datum[] trow = td.next();
			for(Datum s : trow)
			{
				System.out.print(" " + s);
			}
			System.out.println("");
			System.out.println(pageno +"ivalue before the try in lead nodes");
			//Datum[] a = key.createKey(trow);
			//int b = key.hashKey(a);
			//	keyarray[i]= b;
			//  System.out.println("array key"+ keyarray[i] +" "+ "hashkey actual "+ b);

			try{

				z = mgdfile.safePin(pageno-1);
				dbf = new DatumBuffer(z, ((TestDataStream) td).getSchema() );

				dbf.write(trow);
				mgdfile.unpin(pageno-1,true);

				//  System.out.println(i +"ivalue inside the try");

			}
			catch(Exception e){
				mgdfile.unpin(pageno-1);

				System.out.println(pageno +"ivalue when creating a new page in leaf node");
				pageno= pageno+1;
				if(mgdfile.size()-1== pageno ){
					System.out.println("---------------------doubling in if---------------");
					mgdfile.ensureSizeByDoubling(pageno+1);	
				}
				else{
					System.out.println("---------------------doubling in else---------------");
					mgdfile.ensureSizeByDoubling(pageno+1);//made +1
				}
				
				ByteBuffer znew =  mgdfile.safePin(pageno-1);
				DatumBuffer dbf1 = new DatumBuffer(znew, ((TestDataStream) td).getSchema());
				
				dbf1.write(trow);
				mgdfile.unpin(pageno-1,true);
				pagearray[pageno]=pageno;
				//pageno++;
				//	System.out.println("using the page"+ pageno);

			}
		}  
		int leafpages=pageno; // total number of pages (lev0+lev1)
		System.out.println(pageno + "ivalue after first while - after leaf");
		h1.put(level, pageno);
		if(pageno==1)
		{
			mgdfile.ensureSizeByDoubling(pageno+1);
			//indexpagel1=pageno+1;
		}
		else
		{
			indexpagel1=pageno;
		}

		level =1;
		int pagestartatlev1 = pageno;
		System.out.println(pagestartatlev1+" index level one page started at pageno value");
		System.out.println("pinning page no"+indexpagel1+"at level 1");
		ByteBuffer znewl0 = mgdfile.safePin(indexpagel1);
		DatumBuffer dbf1l0 = new DatumBuffer(znewl0, ((TestDataStream) td).getSchema() );
		Datum[] hardzero = new Datum[] {new Datum.Int(0)};
		dbf1l0.write(hardzero);
		mgdfile.unpin(indexpagel1,true);
		while(countforlevel1<pagestartatlev1)
		{
			//  System.out.println(indexpagel1+"indexpagel1");

			for(countforlevel1=1;countforlevel1<pagestartatlev1;countforlevel1++)
			{
				Datum[] pagecount = new Datum[] {new Datum.Int(countforlevel1)};
				ByteBuffer znewkey = mgdfile.pin(countforlevel1);
				DatumBuffer dbfkey = new DatumBuffer(znewkey, ((TestDataStream) td).getSchema() );
				Datum[] a = key.createKey(dbfkey.read(0));
				mgdfile.unpin(countforlevel1);
				boolean pageid =true;
				boolean keyid =true;
				try
				{

					System.out.println("entered level 1 try");
					ByteBuffer znewl1 = mgdfile.safePin(indexpagel1);
					DatumBuffer dbf1l1 = new DatumBuffer(znewl1, ((TestDataStream) td).getSchema() );
					dbf1l1.write(a);
					keyid = false;
					//System.out.println(keyid);
					dbf1l1.write(pagecount);
					mgdfile.unpin(indexpagel1,true);
					pageid = false;
					//System.out.println(pageid);
					System.out.print("[ ");
					for(Datum r: a){
						System.out.print("["+r+ "]");

						for(Datum s: pagecount){
							System.out.print("[ "+s+ "]");
						}	
					}
					System.out.println(" ]");
					//	mgdfile.unpin(indexpagel1,true);
					System.out.println("leaving level1 try");
				}catch(Exception e3){
					System.out.println("Entered level 1 catch");
					System.out.println(keyid);
					System.out.println(pageid);
					mgdfile.unpin(indexpagel1);
					//	System.out.println(i +"ivalue for level one over flow");
					indexpagel1= indexpagel1+1;
					if(mgdfile.size()-1== pageno ){
						mgdfile.ensureSizeByDoubling(pageno+1);	
					}
					else{
						mgdfile.ensureSize(indexpagel1);
					}

					ByteBuffer znew1l1 =  mgdfile.safePin(indexpagel1);
					DatumBuffer dbf1l1ov = new DatumBuffer(znew1l1, ((TestDataStream) td).getSchema());

					dbf1l1ov.write(pagecount);

					mgdfile.unpin(indexpagel1,true);

					pageno=pageno+1;

					pagearray[pageno]=pageno;


					System.out.println("using the page"+ pageno);
				}


			}

			//System.out.println("------------------------------------------level-------------------------------"+((pageno-pagestartatlev1)));

		}
		System.out.println("------------------level 1 is done--------------------");
		pageno=pageno+1;	
		System.out.println("page no at level 1: "+(pageno-pagestartatlev1));
		h1.put(level,(pageno-pagestartatlev1));
		int noplev1=(pageno-pagestartatlev1);


		int indexpage=noplev1;
		int temp=level;
		System.out.println("no of levels so far"+level);
		while(indexpage!=1)
		{	
			if(mgdfile.size()-1== pageno )
				mgdfile.ensureSizeByDoubling(pageno+1);	
			int start=(pageno-indexpage);
			int end=pageno;
			level=level+1;
			temp=level;
			ByteBuffer znewlev = mgdfile.safePin(end);			
			DatumBuffer dbfnewlev = new DatumBuffer(znewlev, ((TestDataStream) td).getSchema() );
			Datum[] hardfirst = new Datum[] {new Datum.Int(start)};
			indexpage=1;
			dbfnewlev.write(hardfirst);
			mgdfile.unpin(end,true);
			for(int i=start+1;i<end;i++)
			{	System.out.println(i+" i inside for ");
			if(mgdfile.size()-1== pageno )
				mgdfile.ensureSizeByDoubling(pageno+1);	
			int fornewpage=i;//change this in case if overflow doesnt work properly
			Datum[] pagecountl2 = new Datum[] {new Datum.Int(i)};
			ByteBuffer zlowlev=null;
			DatumBuffer dbflowlev=null;
			int tempnew=temp;
			while(tempnew!=0)
			{	
				System.out.println("exception i: "+i);

				zlowlev = mgdfile.pin(i);			
				dbflowlev = new DatumBuffer(zlowlev, ((TestDataStream) td).getSchema() );
				Datum[] firstbytes=dbflowlev.read(0);
				mgdfile.unpin(i);
				tempnew=tempnew-1;
				if(tempnew!=0)
					i=firstbytes[0].toInt();

			}
			if(tempnew==0)
			{
				System.out.println("i value in leaf node"+i);
				mgdfile.pin(i);
				Datum[] keyatleaf=dbflowlev.read(0);
				mgdfile.unpin(i);
				ByteBuffer znewl2 = mgdfile.safePin(end);
				System.out.println("end value "+ end);
				DatumBuffer dbf1l2 = new DatumBuffer(znewl2, ((TestDataStream) td).getSchema() );

				if((DatumSerialization.getLength(keyatleaf)+(DatumSerialization.getLength(pagecountl2))  < dbf1l2.remaining()-8))
				{


					dbf1l2.write(keyatleaf);
					dbf1l2.write(pagecountl2);
					for( Datum s : keyatleaf){
						System.out.println("key leaf value "+ s);
						for( Datum r: pagecountl2){
							System.out.println("page count value "+r );
						}

					}




					mgdfile.unpin(end, true);
				}
				else
				{
					mgdfile.unpin(end);
					indexpage=indexpage+1;
					pageno=pageno+1;
					mgdfile.ensureSize(pageno);

					ByteBuffer znewlev1 =  mgdfile.safePin(pageno);
					DatumBuffer dbfnewlev1 = new DatumBuffer(znewlev1, ((TestDataStream) td).getSchema());
					Datum[] pgforov = new Datum[] {new Datum.Int(fornewpage)};
					dbfnewlev1.write(pgforov);
					mgdfile.unpin(pageno,true);


				}
				start=start+1;
				i = start;

			}
			}
			pageno=pageno+1;
			System.out.println(" page no after lev3"+pageno);
			System.out.println(" number of pages at level : "+ level + " are "+ (pageno-end));
			//System.out.println("end of level 3"+end);
			//indexpage=(pageno-end);

			//System.out.println(pageno+ "pageno at level2");
			h1.put(level, (pageno-end));
			System.out.println("level number"+level);
		}

		try{
			// Create file 
			FileWriter fstream = new FileWriter("outISAM.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(level+" ");
			out.write(pageno+" ");
			out.write(h1.get(0)+" ");
			//Close the output stream
			out.close();
		}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}


		mgdfile.flush();
		return null;
					}

	public IndexIterator scan() 
			throws SqlException, IOException
			{
		String InputFile = "outISAM.txt";
		FileInputStream fstream = new FileInputStream(InputFile);
		// Get the object of DataInputStream
		DataInputStream in = new DataInputStream(fstream);
		DatumStreamIterator dsi = new DatumStreamIterator(mgdfile, keySpec.rowSchema());
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		String text="";
		//Read File Line By Line
		while ((strLine = br.readLine()) != null) //loop through each line
		{
			// Print the content on the console

			text +=(strLine+"\n");  // Store the text file in the string
		}
		//System.out.println(text);
		in.close();
		String[] str = text.split(" ");
		int nol = Integer.parseInt(str[0]);
		int root = Integer.parseInt(str[1].trim());
		int datapages = Integer.parseInt(str[2].trim());
		
		dsi.currPage(0);
		dsi.key(keySpec);
		dsi.maxPage(datapages-1);

		System.out.println("no"+datapages);
		dsi.ready();

		return dsi;
		
				
			
	//	throw new SqlException("Unimplemented");
		
			}

	public IndexIterator rangeScanTo(Datum[] toKey)
			throws SqlException, IOException
			{
		DatumStreamIterator dsi = new DatumStreamIterator(mgdfile, keySpec.rowSchema());
		Datum[] key = toKey;
		int position = 1;
		int count=0;
		int length=0;
		int nodenew=0;
		String InputFile = "outISAM.txt";
		FileInputStream fstream = new FileInputStream(InputFile);
		// Get the object of DataInputStream
		DataInputStream in = new DataInputStream(fstream);

		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		String text="";
		//Read File Line By Line
		while ((strLine = br.readLine()) != null) //loop through each line
		{
			// Print the content on the console

			text +=(strLine+"\n");  // Store the text file in the string
		}
		//System.out.println(text);
		in.close();
		String[] str = text.split(" ");
		int nol = Integer.parseInt(str[0]);
		int root = Integer.parseInt(str[1].trim());
		int datapages = Integer.parseInt(str[2].trim());
		System.out.println("root value"+root);
		System.out.println("No of levels"+nol);
	//	System.out.println(mgdfile.size());

		ByteBuffer rootpin=mgdfile.pin(root-1);
		DatumBuffer rootdbf=new DatumBuffer(rootpin, keySpec.keySchema());
		Datum[] a = rootdbf.read(1);
		mgdfile.unpin(root-1);
		System.out.println("Root page length"+rootdbf.length());
		System.out.println(a[0].toInt() + " key in root at 1 " );
		int node=root-1;
		ByteBuffer nodepin=null;
		DatumBuffer nodedb=null;
		while(nol!=0)
		{   nodenew=0;
		 nodepin=mgdfile.pin(node);
		 nodedb=new DatumBuffer(nodepin, keySpec.keySchema());
		//Datum[] anode = nodedb.read(0);
		//mgdfile.unpin(node);


		for(int i=1;i<nodedb.length();i=i+2)
		{
			System.out.println("inside for");
			Datum[] keyvalue=nodedb.read(i);

			if(key[0].toInt()==keyvalue[0].toInt())
			{
				System.out.println("keys equal");
				keyvalue=nodedb.read(i+1);
				nodenew=keyvalue[0].toInt();
				System.out.println("page to search"+nodenew);
				break;
			}
			else if(key[0].toInt()<keyvalue[0].toInt())
			{
				keyvalue=nodedb.read(i-1);
				nodenew=keyvalue[0].toInt();
				break;
			}
			else
			{
				Datum[] keyvalue1=nodedb.read(i+2);
				if(key[0].toInt()<keyvalue[0].toInt() && (key[0].toInt())>(keyvalue1[0].toInt()))
				{
					keyvalue=nodedb.read(i+1);
					nodenew=keyvalue[0].toInt();
					break;

				}
			}
			if(i==nodedb.length()-2 && key[0].toInt()>keyvalue[0].toInt() )
			{
				keyvalue=nodedb.read(i+1);
				nodenew=keyvalue[0].toInt();
				break;
			}
			
			
				
		}
		mgdfile.unpin(node);
		if(nodenew!=0)
		{
		node=nodenew;
		System.out.println("new node value"+node);
		}
		nol=nol-1;
		
		}

		System.out.println("page no "+nodenew);
		ByteBuffer pagepin=mgdfile.pin(nodenew);
		DatumBuffer pagedb=new DatumBuffer(pagepin, keySpec.keySchema());
		Datum[] d1=null;
		Datum[] pagekey=null;
	for( count =0; count<pagedb.length()-1;count++)
	{
		 d1=pagedb.read(count);
		System.out.println("inside for"+count);
		 pagekey = keySpec.createKey(d1);
	
		if( Datum.compareRows(key, pagekey)>=0){
			System.out.println("before break count"+count);
			break;
		}
	}
	while(Datum.compareRows(key, pagekey)>=0 && count<pagedb.length()-1)
	{
		
		d1=pagedb.read(++count);
		pagekey = keySpec.createKey(d1);		
		
	}
	mgdfile.unpin(nodenew);
	System.out.println("i value"+count);
	dsi.currPage(0);
	dsi.key(keySpec);
	dsi.maxPage(nodenew);
	dsi.maxRecord(d1);
	dsi.ready();
	return dsi;
		
		
	//throw new SqlException("Unimplemented");
			}

	public IndexIterator rangeScanFrom(Datum[] fromKey)
			throws SqlException, IOException
			{
		DatumStreamIterator dsi = new DatumStreamIterator(mgdfile, keySpec.rowSchema());
		Datum[] key = fromKey;
		int position = 1;
		int count=0;
		int length=0;
		int nodenew=0;
		String InputFile = "outISAM.txt";
		FileInputStream fstream = new FileInputStream(InputFile);
		// Get the object of DataInputStream
		DataInputStream in = new DataInputStream(fstream);

		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		String text="";
		//Read File Line By Line
		while ((strLine = br.readLine()) != null) //loop through each line
		{
			// Print the content on the console

			text +=(strLine+"\n");  // Store the text file in the string
		}
		//System.out.println(text);
		in.close();
		String[] str = text.split(" ");
		int nol = Integer.parseInt(str[0]);
		int root = Integer.parseInt(str[1].trim());
		int datapages = Integer.parseInt(str[2].trim());
		System.out.println("root value"+root);
		System.out.println("No of levels"+nol);
	//	System.out.println(mgdfile.size());

		ByteBuffer rootpin=mgdfile.pin(root-1);
		DatumBuffer rootdbf=new DatumBuffer(rootpin, keySpec.keySchema());
		Datum[] a = rootdbf.read(1);
		mgdfile.unpin(root-1);
		System.out.println("Root page length"+rootdbf.length());
		System.out.println(a[0].toInt() + " key in root at 1 " );
		int node=root-1;
		ByteBuffer nodepin=null;
		DatumBuffer nodedb=null;
		while(nol!=0)
		{   nodenew=0;
		 nodepin=mgdfile.pin(node);
		 nodedb=new DatumBuffer(nodepin, keySpec.keySchema());
		//Datum[] anode = nodedb.read(0);
		//mgdfile.unpin(node);


		for(int i=1;i<nodedb.length();i=i+2)
		{
			System.out.println("inside for");
			Datum[] keyvalue=nodedb.read(i);

			if(key[0].toInt()==keyvalue[0].toInt())
			{
				System.out.println("keys equal");
				keyvalue=nodedb.read(i+1);
				nodenew=keyvalue[0].toInt();
				System.out.println("page to search"+nodenew);
				break;
			}
			else if(key[0].toInt()<keyvalue[0].toInt())
			{
				keyvalue=nodedb.read(i-1);
				nodenew=keyvalue[0].toInt();
				break;
			}
			else
			{
				Datum[] keyvalue1=nodedb.read(i+2);
				if(key[0].toInt()<keyvalue[0].toInt() && (key[0].toInt())>(keyvalue1[0].toInt()))
				{
					keyvalue=nodedb.read(i+1);
					nodenew=keyvalue[0].toInt();
					break;

				}
			}
			if(i==nodedb.length()-2 && key[0].toInt()>keyvalue[0].toInt() )
			{
				keyvalue=nodedb.read(i+1);
				nodenew=keyvalue[0].toInt();
				break;
			}
			
			
				
		}
		mgdfile.unpin(node);
		if(nodenew!=0)
		{
		node=nodenew;
		System.out.println("new node value"+node);
		}
		nol=nol-1;
		
		}

		System.out.println("page no "+nodenew);
		ByteBuffer pagepin=mgdfile.pin(nodenew);
		DatumBuffer pagedb=new DatumBuffer(pagepin, keySpec.keySchema());
		
	for( count =0; count<pagedb.length()-1;count++){
		Datum[] d1=pagedb.read(count);
		System.out.println("inside for"+count);
		Datum[] pagekey = keySpec.createKey(d1);
	//	int page_key = pagekey[0].toInt();
		if( Datum.compareRows(pagekey, key)>=0){
			System.out.println("before break count"+count);
			break;
		}
	}
	mgdfile.unpin(nodenew);
	System.out.println("i value"+count);
	dsi.currPage(nodenew);
	dsi.key(keySpec);
	dsi.currRecord(count);
	dsi.maxPage(datapages-1);
	dsi.ready();
		return dsi;
	//	throw new SqlException("Unimplemented");
			}

	public IndexIterator rangeScan(Datum[] start, Datum[] end)
			throws SqlException, IOException
		
			{
		DatumStreamIterator dsi = new DatumStreamIterator(mgdfile, keySpec.rowSchema());
		Datum[] key = end;
	
		int position = 1;
		int count=0;
		int length=0;
		int nodenew=0;
		String InputFile = "outISAM.txt";
		FileInputStream fstream = new FileInputStream(InputFile);
		// Get the object of DataInputStream
		DataInputStream in = new DataInputStream(fstream);

		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		String text="";
		//Read File Line By Line
		while ((strLine = br.readLine()) != null) //loop through each line
		{
			// Print the content on the console

			text +=(strLine+"\n");  // Store the text file in the string
		}
		//System.out.println(text);
		in.close();
		String[] str = text.split(" ");
		int nol = Integer.parseInt(str[0]);
		int root = Integer.parseInt(str[1].trim());
		int datapages = Integer.parseInt(str[2].trim());
		System.out.println("root value"+root);
		System.out.println("No of levels"+nol);
	//	System.out.println(mgdfile.size());

		ByteBuffer rootpin=mgdfile.pin(root-1);
		DatumBuffer rootdbf=new DatumBuffer(rootpin, keySpec.keySchema());
		Datum[] a = rootdbf.read(1);
		mgdfile.unpin(root-1);
		System.out.println("Root page length"+rootdbf.length());
		System.out.println(a[0].toInt() + " key in root at 1 " );
		int node=root-1;
		ByteBuffer nodepin=null;
		DatumBuffer nodedb=null;
		while(nol!=0)
		{   nodenew=0;
		 nodepin=mgdfile.pin(node);
		 nodedb=new DatumBuffer(nodepin, keySpec.keySchema());
		//Datum[] anode = nodedb.read(0);
		//mgdfile.unpin(node);


		for(int i=1;i<nodedb.length();i=i+2)
		{
			System.out.println("inside for");
			Datum[] keyvalue=nodedb.read(i);

			if(key[0].toInt()==keyvalue[0].toInt())
			{
				System.out.println("keys equal");
				keyvalue=nodedb.read(i+1);
				nodenew=keyvalue[0].toInt();
				System.out.println("page to search"+nodenew);
				break;
			}
			else if(key[0].toInt()<keyvalue[0].toInt())
			{
				keyvalue=nodedb.read(i-1);
				nodenew=keyvalue[0].toInt();
				break;
			}
			else
			{
				Datum[] keyvalue1=nodedb.read(i+2);
				if(key[0].toInt()<keyvalue[0].toInt() && (key[0].toInt())>(keyvalue1[0].toInt()))
				{
					keyvalue=nodedb.read(i+1);
					nodenew=keyvalue[0].toInt();
					break;

				}
			}
			if(i==nodedb.length()-2 && key[0].toInt()>keyvalue[0].toInt() )
			{
				keyvalue=nodedb.read(i+1);
				nodenew=keyvalue[0].toInt();
				break;
			}
			
			
				
		}
		mgdfile.unpin(node);
		if(nodenew!=0)
		{
		node=nodenew;
		System.out.println("new node value"+node);
		}
		nol=nol-1;
		
		}

		System.out.println("page no "+nodenew);
		ByteBuffer pagepin=mgdfile.pin(nodenew);
		DatumBuffer pagedb=new DatumBuffer(pagepin, keySpec.keySchema());
		Datum[] d1=null;
		Datum[] pagekey=null;
	for( count =0; count<pagedb.length()-1;count++)
	{
		 d1=pagedb.read(count);
		System.out.println("inside for"+count);
		 pagekey = keySpec.createKey(d1);
	
		if( Datum.compareRows(key, pagekey)>=0){
			System.out.println("before break count"+count);
			break;
		}
	}
	while(Datum.compareRows(key, pagekey)>=0 && count<pagedb.length()-1)
	{
		
		d1=pagedb.read(++count);
		pagekey = keySpec.createKey(d1);		
		
	}
	mgdfile.unpin(nodenew);
	System.out.println("i value"+count);
	/*//
	dsi.currPage(0);
	dsi.key(keySpec);
	dsi.maxPage(nodenew);
	dsi.maxRecord(d1);
	dsi.ready();
	//
	 * */
	 
	/////////////////////////////////////////////////////////////////////
	//////////////////
	///////////////////
	
	
	//DatumStreamIterator dsi = new DatumStreamIterator(mgdfile, keySpec.rowSchema());
	Datum[] key2 = start;
	int position2 = 1;
	int count2=0;
	int length2=0;
	int nodenew2=0;
	String InputFile2 = "outISAM.txt";
	FileInputStream fstream2 = new FileInputStream(InputFile2);
	// Get the object of DataInputStream
	DataInputStream in2 = new DataInputStream(fstream2);

	BufferedReader br2 = new BufferedReader(new InputStreamReader(in2));
	String strLine2;
	String text2="";
	//Read File Line By Line
	while ((strLine2 = br2.readLine()) != null) //loop through each line
	{
		// Print the content on the console

		text2 +=(strLine2+"\n");  // Store the text file in the string
	}
	//System.out.println(text);
	in2.close();
	String[] str2 = text2.split(" ");
	int nol2 = Integer.parseInt(str2[0]);
	int root2 = Integer.parseInt(str2[1].trim());
	int datapages2 = Integer.parseInt(str2[2].trim());
	System.out.println("root value"+root2);
	System.out.println("No of levels"+nol2);
//	System.out.println(mgdfile.size());

	ByteBuffer rootpin2=mgdfile.pin(root2-1);
	DatumBuffer rootdbf2=new DatumBuffer(rootpin2, keySpec.keySchema());
	Datum[] a2 = rootdbf2.read(1);
	mgdfile.unpin(root2-1);
	System.out.println("Root page length"+rootdbf2.length());
	System.out.println(a2[0].toInt() + " key in root at 1 " );
	int node2=root2-1;
	ByteBuffer nodepin2=null;
	DatumBuffer nodedb2=null;
	while(nol2!=0)
	{   nodenew2=0;
	 nodepin2=mgdfile.pin(node2);
	 nodedb2=new DatumBuffer(nodepin2, keySpec.keySchema());
	//Datum[] anode = nodedb.read(0);
	//mgdfile.unpin(node);


	for(int i=1;i<nodedb2.length();i=i+2)
	{
		System.out.println("inside for");
		Datum[] keyvalue2=nodedb2.read(i);

		if(key2[0].toInt()==keyvalue2[0].toInt())
		{
			System.out.println("keys equal");
			keyvalue2=nodedb2.read(i+1);
			nodenew2=keyvalue2[0].toInt();
			System.out.println("page to search"+nodenew2);
			break;
		}
		else if(key2[0].toInt()<keyvalue2[0].toInt())
		{
			keyvalue2=nodedb2.read(i-1);
			nodenew2=keyvalue2[0].toInt();
			break;
		}
		else
		{
			Datum[] keyvalue12=nodedb2.read(i+2);
			if(key2[0].toInt()<keyvalue2[0].toInt() && (key2[0].toInt())>(keyvalue12[0].toInt()))
			{
				keyvalue2=nodedb2.read(i+1);
				nodenew2=keyvalue2[0].toInt();
				break;

			}
		}
		if(i==nodedb2.length()-2 && key2[0].toInt()>keyvalue2[0].toInt() )
		{
			keyvalue2=nodedb2.read(i+1);
			nodenew2=keyvalue2[0].toInt();
			break;
		}
		
		
			
	}
	mgdfile.unpin(node2);
	if(nodenew2!=0)
	{
	node2=nodenew2;
	System.out.println("new node value"+node2);
	}
	nol2=nol2-1;
	
	}

	System.out.println("page no "+nodenew2);
	ByteBuffer pagepin2=mgdfile.pin(nodenew2);
	DatumBuffer pagedb2=new DatumBuffer(pagepin2, keySpec.keySchema());
	
for( count2 =0; count2<pagedb2.length()-1;count2++){
	Datum[] d12=pagedb2.read(count2);
	System.out.println("inside for"+count2);
	Datum[] pagekey2 = keySpec.createKey(d12);
//	int page_key = pagekey[0].toInt();
	if( Datum.compareRows(pagekey2, key2)>=0){
		System.out.println("before break count"+count2);
		break;
	}
}
mgdfile.unpin(nodenew2);
System.out.println("i value"+count2);

dsi.currPage(nodenew2);
dsi.key(keySpec);
dsi.currRecord(count2);



dsi.maxPage(nodenew);
dsi.maxRecord(d1);
dsi.ready();

	return dsi;
		
		
		
		
		
		
		
		
		
	//	throw new SqlException("Unimplemented");
			}

	public Datum[] get(Datum[] key)
			throws SqlException, IOException
			{
		int position = 1;
		int length=0;
		int nodenew=0;
		String InputFile = "outISAM.txt";
		FileInputStream fstream = new FileInputStream(InputFile);
		// Get the object of DataInputStream
		DataInputStream in = new DataInputStream(fstream);

		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		String text="";
		//Read File Line By Line
		while ((strLine = br.readLine()) != null) //loop through each line
		{
			// Print the content on the console

			text +=(strLine+"\n");  // Store the text file in the string
		}
		//System.out.println(text);
		in.close();
		String[] str = text.split(" ");
		int nol = Integer.parseInt(str[0]);
		int root = Integer.parseInt(str[1].trim());
		System.out.println("root value"+root);
		System.out.println("No of levels"+nol);
	//	System.out.println(mgdfile.size());

		ByteBuffer rootpin=mgdfile.pin(root-1);
		DatumBuffer rootdbf=new DatumBuffer(rootpin, keySpec.keySchema());
		Datum[] a = rootdbf.read(1);
		mgdfile.unpin(root-1);
		System.out.println("Root page length"+rootdbf.length());
		System.out.println(a[0].toInt() + " key in root at 1 " );
		int node=root-1;
		ByteBuffer nodepin=null;
		DatumBuffer nodedb=null;
		while(nol!=0)
		{   nodenew=0;
		 nodepin=mgdfile.pin(node);
		 nodedb=new DatumBuffer(nodepin, keySpec.keySchema());
		//Datum[] anode = nodedb.read(0);
		//mgdfile.unpin(node);


		for(int i=1;i<nodedb.length();i=i+2)
		{
			System.out.println("inside for");
			Datum[] keyvalue=nodedb.read(i);

			if(key[0].toInt()==keyvalue[0].toInt())
			{
				System.out.println("keys equal");
				keyvalue=nodedb.read(i+1);
				nodenew=keyvalue[0].toInt();
				System.out.println("page to search"+nodenew);
				break;
			}
			else if(key[0].toInt()<keyvalue[0].toInt())
			{
				keyvalue=nodedb.read(i-1);
				nodenew=keyvalue[0].toInt();
				break;
			}
			else
			{
				Datum[] keyvalue1=nodedb.read(i+2);
				if(key[0].toInt()<keyvalue[0].toInt() && (key[0].toInt())>(keyvalue1[0].toInt()))
				{
					keyvalue=nodedb.read(i+1);
					nodenew=keyvalue[0].toInt();
					break;

				}
			}
			if(i==nodedb.length()-2 && key[0].toInt()>keyvalue[0].toInt() )
			{
				keyvalue=nodedb.read(i+1);
				nodenew=keyvalue[0].toInt();
				break;
			}
			
			
				
		}
		mgdfile.unpin(node);
		if(nodenew!=0)
		{
		node=nodenew;
		System.out.println("new node value"+node);
		}
		nol=nol-1;
		
		}
		
		System.out.println("node value"+node);
		System.out.println("nodenew"+nodenew);
		System.out.println("value pinning"+nodenew);
		nodepin=mgdfile.pin(nodenew);
		nodedb=new DatumBuffer(nodepin,keySpec.rowSchema());

		int row_no=nodedb.find(key);
		Datum[] readrecord=nodedb.read(row_no);
		mgdfile.unpin(nodenew);
		Datum[] keyvaluex=keySpec.createKey(readrecord);
		System.out.println(keyvaluex[0].toInt());
		if(Datum.compareRows(keySpec.createKey(readrecord), key)==0)
		{
			System.out.println("------keys matched-------");
			return readrecord;
		}
		else
		{
			System.out.println("-------key matching failed------");
			return null;
		}

		//throw new SqlException("Unimplemented");
		
			}

}