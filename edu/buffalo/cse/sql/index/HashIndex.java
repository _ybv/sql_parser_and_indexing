
package edu.buffalo.cse.sql.index;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import edu.buffalo.cse.sql.Schema;
import edu.buffalo.cse.sql.SqlException;
import edu.buffalo.cse.sql.data.Datum;
import edu.buffalo.cse.sql.data.Datum.CastError;
import edu.buffalo.cse.sql.data.DatumBuffer;
import edu.buffalo.cse.sql.data.DatumSerialization;
import edu.buffalo.cse.sql.data.InsufficientSpaceException;
import edu.buffalo.cse.sql.buffer.BufferManager;
import edu.buffalo.cse.sql.buffer.BufferException;
import edu.buffalo.cse.sql.buffer.ManagedFile;
import edu.buffalo.cse.sql.buffer.FileManager;
import edu.buffalo.cse.sql.plan.PlanNode.Type;
import edu.buffalo.cse.sql.test.TestDataStream;

public class HashIndex implements IndexFile {
	public ManagedFile mgdfile;
	public IndexKeySpec keySpec;

	public HashIndex(ManagedFile file, IndexKeySpec keySpec)
			throws IOException, SqlException
			{
		this.mgdfile=file;
		this.keySpec=keySpec;
		// throw new SqlException("Unimplemented");
			}


	public static HashIndex create(FileManager fm,
			File path,
			Iterator<Datum[]> dataSource,
			IndexKeySpec key,
			int directorySize)
					throws SqlException, IOException
					{
		ManagedFile mgdfile =fm.open(path);
		mgdfile.resize(0);
		//mgdfile.ensureSize(1);
		int i =0;
		int qq=0;
		int oldsize = directorySize; 
		//directoryinfo.directorysizeforget=directorySize;

		DatumBuffer dbf2=null;
		ByteBuffer zovflloop=null;
		int ofpage1=0;
		Iterator<Datum[]> td = dataSource;


		HashMap<Integer, Integer> pagemap = new HashMap<Integer, Integer>();
		edu.buffalo.cse.sql.Schema.Type[] gets = ((TestDataStream) td).getSchema();
		while(td.hasNext())
		{  
			Datum[] trow = td.next();
			for(Datum s : trow)
			{
				System.out.print(" " + s);
			}
			//	System.out.println(" ");

			//System.out.println(directorySize);
			Datum[] a = key.createKey(trow);
			int b = key.hashKey(a);
			int c= b%directorySize;
			mgdfile.resize(oldsize);
			//	System.out.println(mgdfile.size());
			ByteBuffer z = mgdfile.pin(c);
			DatumBuffer dbf = new DatumBuffer(z, ((TestDataStream) td).getSchema() );
			//System.out.println("above if");

			if(!pagemap.containsKey(c))
			{
				pagemap.put(c, c);
				dbf.initialize(4);
				System.out.println("initializing page :"+c);
				z.putInt(0,-1);

			}

			else
			{//System.out.println(z.getInt(0));
				if(DatumSerialization.getLength(trow) < dbf.remaining()-8)
				{
					System.out.println("no overflow row fitting");
					dbf.write(trow);
					mgdfile.unpin(c,true);
					//System.out.println(" after unpin1");

				}

				else 
				{
					//	System.out.println("has overflow page and using an existing overflow page");
					//int ofpage= z.getInt(0);
					//	System.out.println("first 4bytes of "+c+ " which caused overflow "+z.getInt(0));

					int ofpage= z.getInt(0);
					if(z.getInt(0)!=-1){
						while(true)
						{
							if(ofpage!=-1)
							{  


								//System.out.println("ofpage at start"+ofpage);
								zovflloop = mgdfile.pin(ofpage);

								dbf2 = new DatumBuffer(zovflloop , ((TestDataStream) td).getSchema() );

								ofpage1=ofpage;

								ofpage=zovflloop.getInt(0);
								//System.out.println("fisrt 4 bytes of overflow page no "+ ofpage+ "  in loop  at start of while "+ z.getInt(0));

								System.out.println( ofpage +" :node's 4 bytes"+zovflloop.getInt(0));


								mgdfile.unpin(ofpage1);

								System.out.println("page at end of while "+ ofpage);
							}
							else
							{
								break;
							}
						}
						//System.out.println("writing into the overflow page no"+ofpage);
						//	ByteBuffer znew2 = mgdfile.safePin(ofpage);//check ofpage
						//	System.out.println(znew2.getInt(0)+"znew ");


						if(DatumSerialization.getLength(trow) > dbf2.remaining()-8)
						{
							System.out.println("overflow page overflowed");
							//mgdfile.unpin(ofpage1);
							int newsize = oldsize+1;//?
							System.out.println("oldsize"+oldsize);
							//System.out.println("newsize after exception occured"+newsize);
							int x=newsize-1;
							System.out.println(mgdfile.size());
							//	mgdfile.ensureSizeByDoubling(newsize+1);
							mgdfile.resize(newsize);
							System.out.println(mgdfile.size());
							mgdfile.pin(ofpage1);
							zovflloop.putInt(0,newsize-1);
							mgdfile.unpin(ofpage1);
							ByteBuffer znew = mgdfile.pin(newsize-1);
							DatumBuffer dbf1 = new DatumBuffer(znew, ((TestDataStream) td).getSchema() );
							if(dbf1.length()==0){
								dbf1.initialize(4);
								znew.putInt(0,-1);
							}
							System.out.println("New page pinned===========>>>>>>>>>>>>>>>>"+(newsize-1));

							dbf1.write(trow);
							mgdfile.unpin(newsize-1,true);
							oldsize = newsize;
							//	System.out.println(znew.getInt(0));
						}else{
							mgdfile.pin(ofpage1);
							dbf2.write(trow);
							/*for(Datum s : trow)
							{

								System.out.print(" " + s);
								System.out.println("row count"+qq);

							}
							qq++;
							 */
							System.out.println(" overflow page " +ofpage1+"  has enough space");

							mgdfile.unpin(ofpage1,true);

						}

					}	
					else
					{

						System.out.println("first time overflowed occured for"+c);
						int newsize = oldsize+1;///////from +1 to +2
						System.out.println("newsize"+newsize);
						int y= newsize-1;

						z.putInt(0,y);
						mgdfile.unpin(c);
						System.out.println("first 4 bytes of :"+ c +": " +z.getInt(0));
						System.out.println(mgdfile.size());
						mgdfile.resize(newsize);
						System.out.println(mgdfile.size());
						//mgdfile.resize(newsize);
						ByteBuffer znewfirstov = mgdfile.pin(y);
						DatumBuffer dbffirstov = new DatumBuffer(znewfirstov, ((TestDataStream) td).getSchema() );
						if(dbffirstov.length()==0){
							dbffirstov.initialize(4);
							znewfirstov.putInt(0,-1);
						}
						System.out.println("New page pinned===========>>>>>>>>>>>>>>>>"+ y);
						dbffirstov.write(trow);


						System.out.println(znewfirstov.getInt(0)+ " first four bytes of " +y);
						mgdfile.unpin(y,true);
						oldsize=newsize;

					}


				}
			}
			System.out.println(mgdfile.size());
			System.out.println("total number of pages created"+oldsize);
		}mgdfile.flush();
		try{
			// Create file 
			FileWriter fstream = new FileWriter("out.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(directorySize+"");
			//Close the output stream
			out.close();
		}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

		return null;	
					}
	public IndexIterator scan() 
			throws SqlException, IOException
			{
		throw new SqlException("Unimplemented");
			}

	public IndexIterator rangeScanTo(Datum[] toKey)
			throws SqlException, IOException
			{
		throw new SqlException("Unimplemented");
			}

	public IndexIterator rangeScanFrom(Datum[] fromKey)
			throws SqlException, IOException
			{
		throw new SqlException("Unimplemented");
			}

	public IndexIterator rangeScan(Datum[] start, Datum[] end)
			throws SqlException, IOException
			{
		throw new SqlException("Unimplemented");
			}

	public Datum[] get(Datum[] key)
			throws SqlException, IOException
			{
		Datum[] keyspec = key;
		ByteBuffer buf;
		int count =0;
		DatumBuffer dbf;
		Datum[] az=null;
		int hashkey = keySpec.hashKey(key);
		Datum[] readrecord = null;
		int position = 0;
		Datum[] output_tups = null;
		int keylen = keyspec.length;
		String text = "";
		//	try{
		String InputFile = "out.txt";
		FileInputStream fstream = new FileInputStream(InputFile);
		// Get the object of DataInputStream
		DataInputStream in = new DataInputStream(fstream);

		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		//Read File Line By Line

		while ((strLine = br.readLine()) != null) //loop through each line
		{
			// Print the content on the console

			text +=(strLine+"\n");  // Store the text file in the string
		}
		//System.out.println(text);
		in.close();//Close the input stream
		//}
		//catch (Exception e)
		//Catch exception if any
		//			System.out.println("Error: " + e.getMessage());
		//	}
		//String s = text;
		int directorysize= Integer.parseInt(text.trim());
		System.out.println(directorysize);
		int bucknum=hashkey%directorysize;
		System.out.println("hash key "+hashkey);
		int a=bucknum;
		if( count ==0){
		while(a!=-1){
			System.out.println(bucknum+"bucket");
			ByteBuffer z = mgdfile.safePin(bucknum-1);

			buf=mgdfile.safePin(a);
			dbf = new DatumBuffer(buf,keySpec.rowSchema());
			position=dbf.find(keyspec);
			az=dbf.read(position);
			Datum[] compval = keySpec.createKey(az);

			if(keySpec.compare(compval, key)==0)
			{
				output_tups=az;
				mgdfile.unpin(a);
				break;
			}
			else
			{
				mgdfile.unpin(a);
				a=buf.getInt(0);
				
			}
			count++;
		}
		}
		return output_tups;
		
			}

}
